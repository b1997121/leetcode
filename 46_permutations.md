```python
class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        result = [[nums[0]]]

        for i in nums[1:]:
            i_tmp = []
            for j in result :
                for k in range(len(j) + 1) :
                    j_tmp = j[:]
                    j_tmp.insert(k, i)
                    i_tmp.append(j_tmp)
            result = i_tmp[:]

        return result
```
