```python
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        
        sub_str = ""
        str_len = 0
        
        for v in s:
            if v not in sub_str:
                sub_str += v
            else:
                if len(sub_str) > str_len:
                    str_len = len(sub_str)
                sub_str = sub_str[sub_str.index(v) +1:] + v

        if len(sub_str) > str_len:
            str_len = len(sub_str)

        return str_len
```
