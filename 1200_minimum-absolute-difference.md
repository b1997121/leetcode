```python
class Solution:
    def minimumAbsDifference(self, arr: List[int]) -> List[List[int]]:
        arr.sort()
        return_arr = [[arr[0], arr[1]]]
        min = arr[1] - arr[0]
        tmp = 0
        for i in range(2, len(arr)):
            tmp = arr[i] - arr[i-1]
            print(tmp, min)
            if tmp > min:
                continue
            if tmp < min:
                min = tmp
                return_arr = []
            return_arr.append([arr[i-1], arr[i]])
        return return_arr

class Solution:
    def minimumAbsDifference(self, arr: List[int]) -> List[List[int]]:
        min = 0
        tmp = 0
        return_list = []
        for i, v in enumerate(arr[:-1]):
            for v2 in arr[i+1:]:
                tmp = v - v2
                if min == 0:
                    min = abs(tmp)
                    return_list.append([v, v2] if v < v2 else [v2, v])
                else:
                    if abs(tmp) < min or abs(tmp) == min:
                        if abs(tmp) < min:
                            return_list = []
                            min = abs(tmp)
                        return_list.append([v, v2] if v < v2 else [v2, v])
                
        return sorted(return_list)
```