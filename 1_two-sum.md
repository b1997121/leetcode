```python
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        index = {}
        for i, num1 in enumerate(nums):
            num2 = target - num1
            if num2 not in index:
                index[num1] = i
            else:
                return [index[num2], i]

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        num2_index = 0
        for num1_index, num1 in enumerate(nums):
                num2 = target - num1
                if num2 not in nums:
                    continue
                num2_index = nums.index(num2)
                if num1_index == num2_index:
                    continue
                break

            return sorted([num1_index, num2_index])
```